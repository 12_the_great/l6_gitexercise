# L6_GitExercise

We are working on a proximity sensor that can be used in a number of ways, as our project. For exemple, in taps at toilets where you put your hands close to the sensor and water comes out so that you an wash your hands.
The sensor can be added to a tap system so that it works all day and makes sure that a person does not have to make contact with the tap if they want to wash their hands. 
It can also be used at car parks so that when a driver is parking their car, they can avoid collision with the walls. The proximity sensor will have LED lights that will tell the driver how close they are with the walls to avoid collision.
